from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup
import csv

my_url = 'https://www.amazon.com/best-sellers-books-Amazon/zgbs/books'

#Opening the file
filename = "com_book.csv"
f = open(filename, "w")
f.write("Name;URL;Author;Price;Number of Ratings;Average Rating\n")

for j in range(2,7):
    theReq = uReq(my_url)
    page_html = theReq.read()
    theReq.close()

    page_soup = soup(page_html, "html.parser")

    page_containers = page_soup.findAll("li",{"class":"zg_page"})
    book_containers = page_soup.findAll("div",{"class":"zg_itemWrapper"})

    for book in book_containers:
        book_title = book.find("div",{"class":"p13n-sc-truncate p13n-sc-line-clamp-1"})
        if(book_title):
            title = book_title.text.strip()
        else:
            title = "Not available"

        book_url = book.find("a",{"class":"a-link-normal a-text-normal"})
        if(book_url):
            url = "www.amazon.com" + book_url['href']
        else:
            url = "Not available"

        book_author = book.find("div",{"class":"a-row a-size-small"})
        if(book_author):
            author = book_author.text.strip()
        else:
            author = "Not available"

        book_price = book.find("span",{"class":"p13n-sc-price"})
        if(book_price):
            price = book_price.text.strip()
        else:
            price = "Not available"

        book_nRate = (book.find("a",{"class":"a-size-small a-link-normal"}))
        if(book_nRate):
            nRate = book_nRate.text.strip()
        else:
            nRate = "Not available"

        rate = book.i.text
        if rate == "Prime":
            rate = "Not available"

        #Writing the data into the csv file
        book_tuple = title + ";" + url + ";" + author + ";" + price + ";" + nRate + ";" + rate + "\n"

        f.write(book_tuple)

    #Updating url for each page
    if(j-1 < 5):
        my_url = page_containers[j-1].a['href']
